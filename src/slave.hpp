#ifndef KIO_7ZIP_SLAVE_HPP
#define KIO_7ZIP_SLAVE_HPP

#include <memory>

#include <kio/slavebase.h>

#include <karchive.h>

class KArchiveDirectory;
class KArchiveEntry;
class QByteArray;
class QString;
class QUrl;

namespace kio_7zip {

class Slave : public KIO::SlaveBase
{
public:
    Slave(QByteArray const& proto,
          QByteArray const& pool, QByteArray const& app);

    ~Slave() override;

    void listDir(QUrl const& url) override;
    void stat(QUrl const& url) override;
    void get(QUrl const& url) override;

private:
    KArchiveEntry const*
        getEntry(QUrl const&);

    KArchiveDirectory const*
        openArchive(QString const& path);

    void emitMimeType(QString const& name, QByteArray const& chunk);

    bool followSymLink(QUrl const&, KArchiveEntry const&);

private:
    std::unique_ptr<KArchive> m_archive;
};

} // namespace kio_7zip

#endif // KIO_7ZIP_SLAVE_HPP
