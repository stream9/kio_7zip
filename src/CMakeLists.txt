add_library(kio_7zip MODULE
    main.cpp
    debug.cpp
    slave.cpp
)

target_compile_features(kio_7zip PRIVATE cxx_std_17)

target_compile_options(kio_7zip PRIVATE
    -Wall -Wextra -pedantic -Wconversion -Wsign-conversion
)

target_link_libraries(kio_7zip PRIVATE
    KF5::Archive
    KF5::I18n
    KF5::KIOCore
)

set_target_properties(kio_7zip PROPERTIES
    CXX_VISIBILITY_PRESET "hidden"
    VERSION   "1.0"
    SOVERSION "1.0"
    LIBRARY_OUTPUT_NAME "7zip"
)

install(TARGETS kio_7zip DESTINATION ${KDE_INSTALL_PLUGINDIR}/kf5/kio)
