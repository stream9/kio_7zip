#include "slave.hpp"

#include "debug.hpp"

#include <memory>
#include <utility>

#include <QByteArray>
#include <QFileInfo>
#include <QMimeDatabase>
#include <QObject>
#include <QString>
#include <QUrl>

#include <K7Zip>
#include <KArchiveDirectory>
#include <KArchiveEntry>
#include <KArchiveFile>
#include <KIO/UDSEntry>
#include <KLocalizedString>

namespace kio_7zip {

static auto
makeEntry(KArchiveEntry const& entry)
{
    using KIO::UDSEntry;

    UDSEntry e;
    qint64 const size = entry.isFile() ?
        static_cast<KArchiveFile const&>(entry).size() : 0;

    e.fastInsert(UDSEntry::UDS_NAME, entry.name());
    e.fastInsert(UDSEntry::UDS_FILE_TYPE, entry.permissions());
    e.fastInsert(UDSEntry::UDS_SIZE, size);
    e.fastInsert(UDSEntry::UDS_MODIFICATION_TIME, entry.date().toTime_t());
    e.fastInsert(UDSEntry::UDS_ACCESS, entry.permissions());
    e.fastInsert(UDSEntry::UDS_USER, entry.user());
    e.fastInsert(UDSEntry::UDS_GROUP, entry.group());
    e.fastInsert(UDSEntry::UDS_LINK_DEST, entry.symLinkTarget());

    return e;
}

static auto
makeCurrentDirectoryEntry(KArchive const& archive)
{
    using KIO::UDSEntry;

    QFileInfo info { archive.fileName() };
    UDSEntry e;

    e.fastInsert(UDSEntry::UDS_NAME, "." );
    e.fastInsert(UDSEntry::UDS_FILE_TYPE, S_IFDIR );
    e.fastInsert(KIO::UDSEntry::UDS_MODIFICATION_TIME,
                                    info.lastModified().currentSecsSinceEpoch());
    e.fastInsert(KIO::UDSEntry::UDS_USER, info.owner());
    e.fastInsert(KIO::UDSEntry::UDS_GROUP, info.group());

    return e;
}

// @return { file_name, entry_name }
// @return {} when there in't a file in the path
static std::pair<QString, QString>
splitPath(QString const& path)
{
    int idx = path.indexOf('/', 1);

    while (true) {
        auto const& prefix = path.left(idx);
        QFileInfo f { prefix };

        if (!f.exists()) {
            break;
        }
        else if (f.isFile()) {
            QString const& entryPath = idx == -1 ? "/" : path.mid(idx);

            return std::make_pair(prefix, entryPath);
        }
        else if (f.isSymLink()) {
            return splitPath(f.symLinkTarget());
        }

        if (idx == -1) break;
        idx = path.indexOf('/', idx + 1);
    };

    return {};
}

//
// Slave
//
Slave::
Slave(QByteArray const& proto,
        QByteArray const& pool, QByteArray const& app)
    : KIO::SlaveBase { proto, pool, app }
{}

Slave::~Slave() = default;

void Slave::
listDir(QUrl const& url)
{
    auto const* entry = getEntry(url);
    if (!entry) return;

    if (!entry->isDirectory()) {
        this->error(KIO::ERR_IS_FILE, url.toDisplayString());
        return;
    }

    auto const* dir = static_cast<KArchiveDirectory const*>(entry);
    auto const& entries = dir->entries();

    this->totalSize(static_cast<KIO::filesize_t>(entries.size()));

    if (!entries.contains(".")) {
        this->listEntry(makeCurrentDirectoryEntry(*m_archive));
    }

    for (auto const& name: entries) {
        auto const* entry = dir->entry(name);
        if (!entry) {
            this->error(KIO::ERR_SLAVE_DEFINED,
               i18n("Can't get archive entry: %1", name) );
            return;
        }

        this->listEntry(makeEntry(*entry));
    }

    this->finished();
}

void Slave::
stat(QUrl const& url)
{
    auto* const entry = getEntry(url);
    if (!entry) return;

    this->statEntry(makeEntry(*entry));

    this->finished();
}

void Slave::
get(QUrl const& url)
{
    auto* const entry = getEntry(url);
    if (!entry) return;

    if (entry->isDirectory()) {
        this->error(KIO::ERR_IS_DIRECTORY, url.toDisplayString());
        return;
    }

    if (followSymLink(url, *entry)) {
        this->finished();
        return;
    }

    auto const& fileEntry = static_cast<KArchiveFile const&>(*entry);

    std::unique_ptr<QIODevice> io { fileEntry.createDevice() };
    if (!io) {
        this->error(KIO::ERR_CANNOT_OPEN_FOR_READING, url.toDisplayString());
        return;
    }
    if (!io->isOpen()) {
        if (!io->open(QIODevice::ReadOnly)) {
            this->error(KIO::ERR_CANNOT_OPEN_FOR_READING, url.toDisplayString());
            return;
        }
    }

    this->totalSize(static_cast<KIO::filesize_t>(fileEntry.size()));

    int64_t processed = 0;
    bool first = true;

    while (!io->atEnd()) {
        auto constexpr chunkSize = 1024 * 1024;

        auto const chunk = io->read(chunkSize);
        if (chunk.isEmpty()) {
            this->error(KIO::ERR_CANNOT_READ, url.toDisplayString());
            return;
        }

        if (first) {
            emitMimeType(fileEntry.name(), chunk);
            first = false;
        }

        this->data(chunk);

        processed += chunk.size();
        this->processedSize(static_cast<KIO::filesize_t>(processed));
    }

    if (processed != fileEntry.size()) {
        auto const& msg = QString(
            "Filesize mismatch detected on %1\n"
            "Size on header: %2, actural size: %3"
        ).arg(url.toDisplayString())
         .arg(fileEntry.size())
         .arg(processed);

        this->warning(msg);
    }

    this->data({});
    this->finished();
}

KArchiveEntry const* Slave::
getEntry(QUrl const& url)
{
    auto const& [filePath, entryPath] = splitPath(url.path());
    if (filePath.isEmpty()) {
        this->error(KIO::ERR_DOES_NOT_EXIST, url.toDisplayString());
        return nullptr;
    }

    auto* const root = openArchive(filePath);
    if (!root) return nullptr;

    QString const& p = entryPath.isEmpty() ? "/" : entryPath;

    auto* const entry = root->entry(p);

    if (!entry) {
        this->error(KIO::ERR_DOES_NOT_EXIST, entryPath);
        return nullptr;
    }

    return entry;
}

KArchiveDirectory const* Slave::
openArchive(QString const& filePath)
{
    m_archive = std::make_unique<K7Zip>(filePath);
    if (!m_archive->open(QIODevice::ReadOnly)) {
        this->error(KIO::ERR_CANNOT_OPEN_FOR_READING, filePath);

        m_archive.reset();
        return nullptr;
    }

    auto const* root = m_archive->directory();
    if (!root) {
        this->error(KIO::ERR_SLAVE_DEFINED,
                i18n("Can't get root directory: %1", filePath) );
        return nullptr;
    }

    return root;
}

void Slave::
emitMimeType(QString const& name, QByteArray const& chunk)
{
    QMimeDatabase db;
    auto const& mime =
        db.mimeTypeForFileNameAndData(name, chunk);

    this->mimeType(mime.name());
}

bool Slave::
followSymLink(QUrl const& url, KArchiveEntry const& entry)
{
    auto const& target = entry.symLinkTarget();

    if (!target.isEmpty()) {
        auto const& newUrl = url.resolved(target);

        this->redirection(newUrl);
        return true;;
    }

    return false;
}

} // namespace kio_7zip
