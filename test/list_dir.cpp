#include "archive.hpp"
#include "uds_entry.hpp"
#include "utility.hpp"

#include <memory>

#include <boost/test/unit_test.hpp>

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrl>

#include <K7Zip>
#include <KIO/UDSEntry>
#include <KIO/ListJob>

BOOST_AUTO_TEST_SUITE(kio_7zip_)
BOOST_AUTO_TEST_SUITE(listDir_)

    // execute listDir on <url> and compare result with <json>
    void
    listDirAndCheck(QUrl const& url, QJsonArray const& json)
    {
        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        KIO::UDSEntryList entries;

        QObject::connect(job.get(), &KIO::ListJob::entries,
            [&](KIO::Job*, KIO::UDSEntryList const& list) {
                entries = list;
            }
        );

        BOOST_REQUIRE(job->exec());

        checkUDSEntries(entries, json);
    }

    BOOST_AUTO_TEST_CASE(ok1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            },
            {
                "type": "file",
                "name": "file2",
                "permission": "0100666",
                "content": "content2"
            }
        ])");

        QString const filename { "test1.7z" };

        auto const& archive = makeArchive(filename, data);

        auto const& url = makeUrl(filename);

        listDirAndCheck(url, data);
    }

    BOOST_AUTO_TEST_CASE(ok2_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            },
            {
                "type": "file",
                "name": "file2",
                "permission": "0100644",
                "content": "content2"
            },
            {
                "type": "directory",
                "name": "dir1",
                "permission": "040644",
                "files": [
                    {
                        "type": "file",
                        "name": "file3",
                        "permission": "0100644",
                        "content": "content3"
                    },
                    {
                        "type": "directory",
                        "name": "dir2",
                        "permission": "040644",
                        "files": [
                            {
                                "type": "file",
                                "name": "file4",
                                "permission": "0100644",
                                "content": "content4"
                            }
                        ]
                    }
                ]
            }
        ])");

        // root
        QString const filename { "test2.7z" };
        auto const& archive = makeArchive(filename, data);
        auto const url = makeUrl(filename);

        listDirAndCheck(url, data);

        // dir1
        auto const& dir1 = data[2].toObject();
        auto const& data1 = dir1["files"].toArray();

        auto url1 = url;
        url1.setPath(url1.path() + "/dir1");

        listDirAndCheck(url1, data1);

        // dir2
        auto const& dir2 = data1[1].toObject();
        auto const& data2 = dir2["files"].toArray();

        auto url2 = url1;
        url2.setPath(url2.path() + "/dir2");

        listDirAndCheck(url2, data2);
    }

    BOOST_AUTO_TEST_CASE(error1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            }
        ])");

        QString const filename { "test1.7z" };

        auto const& archive = makeArchive(filename, data);

        auto const& url = makeUrl(filename + "/file1");

        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_IS_FILE);
    }

    BOOST_AUTO_TEST_CASE(error2_)
    {
        QString const filename { "test1.7z" };

        QDir::current().remove(filename);

        auto const& url = makeUrl(filename);

        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

    BOOST_AUTO_TEST_CASE(error3_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            }
        ])");

        QString const filename { "test1.7z" };
        auto const& archive = makeArchive(filename, data);
        auto const& url = makeUrl(filename + "/file3");

        std::unique_ptr<KIO::ListJob> job {
            KIO::listDir(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

BOOST_AUTO_TEST_SUITE_END() // listDir_
BOOST_AUTO_TEST_SUITE_END() // kio_7zip_
