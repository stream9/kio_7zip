#ifndef KIO_7ZIP_TEST_ARCHIVE_HPP
#define KIO_7ZIP_TEST_ARCHIVE_HPP

class QString;
class QJsonArray;

class K7Zip;

K7Zip
makeArchive(QString const& filename, QJsonArray const& json);

#endif // KIO_7ZIP_TEST_ARCHIVE_HPP
