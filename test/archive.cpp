#include "utility.hpp"

#include <sys/stat.h>

#include <boost/test/unit_test.hpp>

#include <QString>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QDir>
#include <QDebug>

#include <K7Zip>

static void addEntry(K7Zip& archive, QString const& dir, QJsonObject const& obj);

static mode_t
getPermission(QJsonObject const& obj)
{
    auto const& s = obj["permission"].toString();
    bool ok = false;

    auto const result = s.toInt(&ok, 8);
    BOOST_REQUIRE(ok);

    return static_cast<mode_t>(result);
}

static void
addFile(K7Zip& archive, QString const& dir, QJsonObject const& obj)
{
    auto const& name = obj["name"].toString();
    auto const permission = getPermission(obj);

    auto const& path = dir.isEmpty() ? name : (dir + "/" + name);

    auto const& content = obj["content"].toString().toUtf8();

    archive.writeFile(path, content.constData(), permission);
}

static void
addDirectory(K7Zip& archive, QString const& dir, QJsonObject const& obj)
{
    auto const& name = obj["name"].toString();
    auto const permission = getPermission(obj);

    auto const& path = dir.isEmpty() ? name : (dir + "/" + name);

    archive.writeDir(path, "", "", permission);

    for (auto const& value: obj["files"].toArray()) {
        auto const& entry = value.toObject();

        addEntry(archive, dir + "/" + name, entry);
    }
}

static void
addEntry(K7Zip& archive, QString const& dir, QJsonObject const& obj)
{
    auto const& type = obj["type"].toString();

    if (type == "file") {
        addFile(archive, dir, obj);
    }
    else if (type == "directory") {
        addDirectory(archive, dir, obj);
    }
    else {
        BOOST_REQUIRE(false && "unknown type");
    }
}

K7Zip
makeArchive(QString const& filename, QJsonArray const& data)
{
    QDir::current().remove(filename);

    K7Zip archive { filename };
    BOOST_REQUIRE(archive.open(QIODevice::WriteOnly));

    for (auto const& value: data) {
        auto const& obj = value.toObject();

        addEntry(archive, "", obj);
    }

    BOOST_REQUIRE(archive.close());

    return archive;
}
