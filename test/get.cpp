#include "archive.hpp"

#include "uds_entry.hpp"
#include "utility.hpp"

#include <boost/test/unit_test.hpp>

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrl>

#include <K7Zip>
#include <KIO/UDSEntry>
#include <KIO/TransferJob>

BOOST_AUTO_TEST_SUITE(kio_7zip_)
BOOST_AUTO_TEST_SUITE(get_)

    struct Job
    {
        std::unique_ptr<KIO::TransferJob> job;

        int numChunk = 0;
        uint64_t totalSize = 0, processedSize = 0;
        QByteArray data;
        QString mimeType;

        void get(QUrl const& url)
        {
            job.reset(
                KIO::get(
                    url,
                    KIO::NoReload,
                    KIO::HideProgressInfo
                )
            );

            QObject::connect(job.get(), &KIO::TransferJob::totalSize,
                [&](KJob*, qulonglong const v) {
                    this->totalSize = v;
                });

            QObject::connect(job.get(), &KIO::TransferJob::processedSize,
                [&](KJob*, qulonglong const v) {
                    this->processedSize = v;
                });

            QObject::connect(job.get(),
                qOverload<KIO::Job*, QString const&>(&KIO::TransferJob::mimetype),
                [&](KIO::Job*, QString const& v) {
                    this->mimeType = v;
                });

            QObject::connect(job.get(), &KIO::TransferJob::data,
                [&](KIO::Job*, QByteArray const& buf) {
                    ++this->numChunk;
                    this->data.append(buf);
                });

            //BOOST_REQUIRE(job->exec());
            if (!job->exec()) {
                qDebug() << job->errorString();
            }

        }
    };

    void
    getAndCheck(QUrl const& url, QJsonObject const& obj)
    {
        Job job;
        job.get(url);

        auto const& content = obj["content"].toString();

        BOOST_TEST(job.numChunk > 0);
        BOOST_TEST(job.totalSize == content.size());
        BOOST_TEST(job.processedSize == content.size());
        BOOST_TEST(job.mimeType == "text/plain");
        BOOST_TEST(job.data == content);
    }

    BOOST_AUTO_TEST_CASE(ok1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            },
            {
                "type": "directory",
                "name": "dir1",
                "permission": "040644",
                "files": [
                    {
                        "type": "file",
                        "name": "file2",
                        "permission": "0100644",
                        "content": "content2"
                    }
                ]
            }
        ])");

        QString const filename { "test1.7z" };
        makeArchive(filename, data);

        auto const& url1 = makeUrl(filename + "/file1");
        auto const& obj1 = data[0].toObject();

        getAndCheck(url1, obj1);

        auto const& url2 = makeUrl(filename + "/dir1/file2");
        auto const& dir1 = (data[1].toObject())["files"].toArray();
        auto const& obj2 = dir1[0].toObject();

        getAndCheck(url2, obj2);
    }

    BOOST_AUTO_TEST_CASE(large_file_)
    {
        QString const filename { "test3.7z" };

        QDir::current().remove(filename);
        K7Zip archive { filename };

        BOOST_REQUIRE(archive.open(QIODevice::WriteOnly));

        QByteArray buf;
        buf.fill('X', 3 * 1024 * 1024);

        archive.writeFile("file1", buf);

        BOOST_REQUIRE(archive.close());

        auto const& url = makeUrl(filename + "/file1");

        Job job;
        job.get(url);

        BOOST_TEST(job.numChunk > 0);
        BOOST_TEST(job.totalSize == buf.size());
        BOOST_TEST(job.processedSize == buf.size());
        BOOST_TEST(job.mimeType == "text/plain");
        BOOST_TEST(job.data == buf);
    }

    BOOST_AUTO_TEST_CASE(symlink_)
    {
        QString const& filename { "test4.7z" };

        QDir::current().remove(filename);
        K7Zip archive { filename };

        BOOST_REQUIRE(archive.open(QIODevice::WriteOnly));

        QByteArray buf { "foo" };
        archive.writeFile("file1", buf);
        archive.writeSymLink("file2", "file1");

        BOOST_REQUIRE(archive.close());

        auto const& url = makeUrl(filename + "/file2");

        Job job;
        job.get(url);

        BOOST_TEST(job.numChunk > 0);
        BOOST_TEST(job.totalSize == buf.size());
        BOOST_TEST(job.processedSize == buf.size());
        BOOST_TEST(job.mimeType == "text/plain");
        BOOST_TEST(job.data == buf);
    }

    BOOST_AUTO_TEST_CASE(error1_)
    {
        QString const filename { "test1.7z" };

        QDir::current().remove(filename);

        auto const& url = makeUrl(filename + "/file1");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

    BOOST_AUTO_TEST_CASE(error2_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            }
        ])");

        QString const filename { "test1.7z" };
        auto const& archive = makeArchive(filename, data);
        auto const& url = makeUrl(filename + "/file3");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

    BOOST_AUTO_TEST_CASE(error3_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "directory",
                "name": "dir1",
                "permission": "040644",
                "files": [
                    {
                        "type": "file",
                        "name": "file2",
                        "permission": "0100644",
                        "content": "content2"
                    }
                ]
            }
        ])");

        QString const filename { "test1.7z" };
        auto const& archive = makeArchive(filename, data);
        auto const& url = makeUrl(filename + "/dir1");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_IS_DIRECTORY);
    }

    BOOST_AUTO_TEST_CASE(error4_)
    {
        QString const& filename { "test1.7z" };

        QDir::current().remove(filename);
        K7Zip archive { filename };

        BOOST_REQUIRE(archive.open(QIODevice::WriteOnly));

        QByteArray buf { "foo" };
        archive.writeFile("file1", buf);
        archive.writeSymLink("file2", "file3");

        BOOST_REQUIRE(archive.close());

        auto const& url = makeUrl(filename + "/file2");

        std::unique_ptr<KIO::TransferJob> job {
            KIO::get(url, KIO::NoReload, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

BOOST_AUTO_TEST_SUITE_END() // get_
BOOST_AUTO_TEST_SUITE_END() // kio_7zip_
