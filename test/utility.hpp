#ifndef KIO_7ZIP_TEST_UTILITY_HPP
#define KIO_7ZIP_TEST_UTILITY_HPP

#include <iostream>

#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QString>
#include <QUrl>

#include <boost/test/unit_test.hpp>

inline std::ostream&
operator<<(std::ostream& os, QString const& s)
{
    return os << s.toUtf8().constData();
}

inline QUrl
makeUrl(QString const& filename)
{
    QUrl url;
    auto const& path = QDir::current().absoluteFilePath(filename);

    url.setScheme("k7zip");
    url.setPath(path);

    return url;
}

inline QJsonArray
makeJsonData(QByteArray const& json)
{
    QJsonParseError err;
    auto const& doc = QJsonDocument::fromJson(json, &err);

    BOOST_TEST_INFO(err.errorString());
    BOOST_REQUIRE(!doc.isNull());

    return doc.array();
}

#endif // KIO_7ZIP_TEST_UTILITY_HPP
