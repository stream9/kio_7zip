#define BOOST_TEST_MODULE kio_7zip
#define BOOST_TEST_NO_MAIN
#include <boost/test/unit_test.hpp>

#include <QCoreApplication>

int
main(int argc, char* argv[])
{
    QCoreApplication app { argc, argv };

    return boost::unit_test::unit_test_main(
        init_unit_test, argc, argv
    );
}
