#include "archive.hpp"
#include "uds_entry.hpp"
#include "utility.hpp"

#include <memory>

#include <boost/test/unit_test.hpp>

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrl>

#include <K7Zip>
#include <KIO/UDSEntry>
#include <KIO/StatJob>

BOOST_AUTO_TEST_SUITE(kio_7zip_)
BOOST_AUTO_TEST_SUITE(stat_)

    void
    statAndCheck(QUrl const& url, QJsonObject const& data)
    {
        std::unique_ptr<KIO::StatJob> job {
            KIO::stat(
                url,
                KIO::StatJob::SourceSide,
                2,
                KIO::HideProgressInfo
            )
        };

        BOOST_REQUIRE(job->exec());

        auto const& entry = job->statResult();

        checkUDSEntry(entry, data);
    }

    BOOST_AUTO_TEST_CASE(ok1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            },
            {
                "type": "file",
                "name": "file2",
                "permission": "0100666",
                "content": "content2"
            }
        ])");

        QString const filename { "test1.7z" };
        makeArchive(filename, data);
        auto const& url = makeUrl(filename + "/file1");

        auto const& obj = data[0].toObject();

        statAndCheck(url, obj);
    }

    BOOST_AUTO_TEST_CASE(error1_)
    {
        auto const& data = makeJsonData(R"([
            {
                "type": "file",
                "name": "file1",
                "permission": "0100644",
                "content": "content1"
            }
        ])");

        QString const filename { "test1.7z" };
        makeArchive(filename, data);
        auto const& url = makeUrl(filename + "/file3");

        std::unique_ptr<KIO::StatJob> job {
            KIO::stat(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

    BOOST_AUTO_TEST_CASE(error2_)
    {
        QString const filename { "test1.7z" };

        QDir::current().remove(filename);

        auto const& url = makeUrl(filename);

        std::unique_ptr<KIO::StatJob> job {
            KIO::stat(url, KIO::HideProgressInfo)
        };

        BOOST_REQUIRE(!job->exec());

        BOOST_TEST(job->error() == KIO::ERR_DOES_NOT_EXIST);
    }

BOOST_AUTO_TEST_SUITE_END() // stat_
BOOST_AUTO_TEST_SUITE_END() // kio_7zip_
