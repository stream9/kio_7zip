## Introduction

KIO slave for 7zip archive file.   
You can use it with KDE file managers (dolphin, etc...)

## Requirement

- C++17 compiler
- cmake
- KDE Frameworks 5
- boost library